import axios, { AxiosInstance } from "axios"
import getAuthorization from "./get-authorization"
import { Container } from "typedi"

export type Headers = {
  "Content-Type": string
  Authorization?: string
  [key: string]: string | undefined
}

interface ApiClientParams {
  token?: string
  API_CLIENT_NAME?: string
  headers?: Partial<Headers>
  localStorageTokenKey?: string
}

const apiClient = ({
  token,
  API_CLIENT_NAME = "API_BASE_URL",
  headers = {},
  localStorageTokenKey,
}: ApiClientParams = {}): AxiosInstance => {
  const clientHeaders: Partial<Headers> = {
    "Content-Type": "application/json",
    ...headers,
  }

  const authorization = getAuthorization({ token, localStorageTokenKey })

  if (authorization) {
    clientHeaders["Authorization"] = authorization
  }

  return axios.create({
    baseURL: Container.get(API_CLIENT_NAME),
    headers: clientHeaders,
  })
}

export default apiClient
