import ServerNotReachableError from "./server-not-reachable-error"
import { AxiosResponse, AxiosRequestConfig } from "axios"

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AxiosInstanceGetterFunctionType = <T = any, R = AxiosResponse<T>>(
  url: string,
  config?: AxiosRequestConfig
) => Promise<R>
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AxiosInstanceSetterFunctionType = <T = any, R = AxiosResponse<T>>(
  url: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data?: any,
  config?: AxiosRequestConfig
) => Promise<R>

type FunctionType =
  | AxiosInstanceGetterFunctionType
  | AxiosInstanceSetterFunctionType

const call = async (
  f: FunctionType,
  url: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ...args: any[]
): Promise<AxiosResponse> => {
  let response
  try {
    if (args.length === 1) {
      const typedF = f as AxiosInstanceGetterFunctionType
      response = await typedF(url, args[0])
    } else if (args.length === 2) {
      const typedF = f as AxiosInstanceSetterFunctionType
      response = await typedF(url, args[0], args[1])
    } else {
      throw new Error("Argument Error")
    }
  } catch (e) {
    // message.destroy()

    if (process.env.NODE_ENV === "development") {
      Object.keys(e)
        .filter((k) => k !== "toJSON")
        .forEach((k) => console.log(`e.${k}`, e[k]))
    }

    if (!e.isAxiosError) {
      throw e
    }

    response = e.response

    if (!response) {
      throw new ServerNotReachableError("Maybe server is not running")
    }

    if (response.status >= 403 && response.status < 500) {
      return response
    }

    if (response.status >= 500) {
      throw e
    }
  }
  return response
}

export default call
