export type Replacements = {
  [key: string]: string
}

const createUrl = (url: string, replacements: Replacements = {}): string => {
  let createdUrl = url
  Object.keys(replacements).forEach(
    (key) => (createdUrl = createdUrl.replace(`:${key}`, replacements[key]))
  )
  return createdUrl
}

export default createUrl
