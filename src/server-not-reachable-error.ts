export default class ServerNotReachableError extends Error {
  constructor(message: string) {
    super(message)
    this.name = "ServerNotReachableError"
  }
}
