import { Container } from "typedi"

type ConfigurationOptions = {
  API_CLIENT_NAME: string
  API_BASE_URL: string
  LOCALSTORAGE_TOKEN_KEY?: string
}

const configure = ({
  API_CLIENT_NAME = "API_BASE_URL",
  API_BASE_URL,
  LOCALSTORAGE_TOKEN_KEY,
}: ConfigurationOptions): void => {
  Container.set(API_CLIENT_NAME, API_BASE_URL)
  Container.set("LOCALSTORAGE_TOKEN_KEY", LOCALSTORAGE_TOKEN_KEY)
}

export default configure
