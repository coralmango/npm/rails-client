import { deserialise } from "kitsu-core"

import apiClient from "./get-client"
import call from "./call"
import createUrl, { Replacements } from "./create-url"
import { AxiosResponse } from "axios"
import type { Headers } from "./get-client"

const localDeserialize = (object: unknown) => {
  if (!object) return null
  return deserialise(object)
}

type MethodLower = "get" | "post" | "put" | "patch" | "delete"
type MethodUpper = "GET" | "POST" | "PUT" | "PATCH" | "DELETE"

type ApiEndpoint = {
  method: MethodLower | MethodUpper
  url: string
}

export type ApiCallParameters = {
  apiEndpoint: ApiEndpoint
  apiUrlReplacements: Replacements
  apiData: unknown
  apiClientName?: string
  apiClientHeaders: Partial<Headers>
  localStorageTokenKey?: string
}

type RetvalType = {
  [key: string]: unknown
}

const generic = async ({
  apiClientName,
  apiEndpoint,
  apiUrlReplacements = {},
  apiData = {},
  apiClientHeaders = {},
  localStorageTokenKey,
}: ApiCallParameters): Promise<unknown> => {
  if (!apiEndpoint) throw new Error("apiEndpoint is required")
  if (!apiEndpoint.method) throw new Error("apiEndpoint.method is required")
  if (!apiEndpoint.url) throw new Error("apiEndpoint.url is required")

  const { method: methodRaw, url: urlRaw } = apiEndpoint
  const method: MethodLower = methodRaw.toLowerCase() as MethodLower
  let response: AxiosResponse
  try {
    const url = createUrl(urlRaw, apiUrlReplacements)
    response = await call(
      apiClient({
        API_CLIENT_NAME: apiClientName,
        headers: apiClientHeaders,
        localStorageTokenKey,
      })[method],
      url,
      apiData
    )
  } catch (e) {
    if (e.name === "ServerNotReachableError") {
      // message.error("Unable to reach server, please try after some time")
      return
    }
    // message.error("An error occured, please try after some time")
    throw e
  }

  if (response?.data?.data) {
    // Handle single data deserialization
    return localDeserialize(response.data)
  }
  if (response.data.errors) {
    // Handle single data errors deserialization
    return localDeserialize(response.data)
  }

  const dataKeys = Object.keys(response.data)
  const retval: RetvalType = {}
  const promises = dataKeys.map(
    async (key) => (retval[key] = await localDeserialize(response.data[key]))
  )
  await Promise.all(promises)
  return retval
}

export default generic
