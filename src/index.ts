import "reflect-metadata"

import ServerNotReachableError from "./server-not-reachable-error"
import generic, { ApiCallParameters } from "./call-deserialize"
import configure from "./configure"
import createUrl from "./create-url"

type FetchFunction = (args: ApiCallParameters) => Promise<unknown>

type Client = {
  fetch: FetchFunction
}

interface ClientParams {
  apiClientName?: string
  localStorageTokenKey?: string
}

const client = ({
  apiClientName,
  localStorageTokenKey,
}: ClientParams): Client => ({
  fetch: ({ ...args }: ApiCallParameters) =>
    generic({ ...args, apiClientName, localStorageTokenKey }),
})

export { ServerNotReachableError, client, configure, createUrl }
