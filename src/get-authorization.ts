import { fetch } from "./local-storage"
import { Container } from "typedi"

const DEFAULT_LOCALSTORAGE_TOKEN_KEY = "auth-token"

interface GetAuthorizationParams {
  token?: string
  localStorageTokenKey?: string
}

const getAuthorization = ({
  token,
  localStorageTokenKey,
}: GetAuthorizationParams = {}): string | null => {
  if (token) return `Bearer ${token}`

  const storedToken = fetch({
    key:
      localStorageTokenKey ||
      Container.get("LOCALSTORAGE_TOKEN_KEY") ||
      DEFAULT_LOCALSTORAGE_TOKEN_KEY,
  })
  if (!storedToken) return null

  return `Bearer ${storedToken}`
}

export default getAuthorization
