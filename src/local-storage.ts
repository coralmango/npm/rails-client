type KeyType = string
type ValueType = unknown

type StoragePair = {
  key: KeyType
  value: ValueType
}

export const store = ({ key, value }: StoragePair): void => {
  try {
    localStorage.setItem(key, JSON.stringify(value))
  } catch (error) {
    console.log(error)
    return
  }
}

export const fetch = ({ key }: { key: KeyType }): ValueType => {
  const value = localStorage.getItem(key)
  if (!value) return null

  return JSON.parse(value)
}
